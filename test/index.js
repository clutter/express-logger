/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const EventEmitter = require("events");
const express = require("express");
const reqId = require("@parthar/express-request-id");
const JsonStreamLogger = require("@parthar/json-stream-logger");
const request = require("supertest");
const stream = require("stream");
const expressLogger = require("../");

const app = express();
const emitter = new EventEmitter();

class TestStream extends stream.Writable {
    write(json) {
        emitter.emit("log", json);

        return true;
    }
}

const logger = new JsonStreamLogger({
    "streams": [new TestStream()]
});

// setup req-id
app.use(reqId({
    "key": "_id"
}));
// setup logger
app.use(expressLogger(logger, {
    "id": function (req) {
        return req._id;
    }
}));
// respond with different levels
app.get("/", function (req, res) {
    res.sendStatus(req.query.sts);
});

// tests start here
function assertReq(data) {
    assert.ok(data.req);
    assert.strictEqual(data.msg, "HTTP Request");
    assert.ok(data.req.url);
    assert.ok(data.req.headers);
    assert.ok(data.req.method);
    assert.ok(data.req.httpVersion);
    assert.ok(data.req.query);
    assert.ok(!data.ms);
}

function assertRes(data) {
    assert.ok(data.res);
    assert.strictEqual(data.msg, "HTTP Response");
    assert.ok(data.res.headers);
    assert.ok(data.res.statusCode);
    assert.ok(data.ms);
}

before(function (done) {
    done();
});

describe("express-logger", function () {
    it("should export express middleware", function (done) {
        assert.strictEqual(typeof expressLogger(), "function");
        assert.strictEqual(expressLogger().length, 3);
        done();
    });
    it("HTTP-200 should log info-level message", function (done) {
        let count = 0;

        emitter.on("log", function (data) {
            count++;
            assert.ok(data.id);
            if (count === 1) {
                assertReq(data);
                assert.strictEqual(data.level, "info");
            } else if (count === 2) {
                assertRes(data);
                assert.strictEqual(data.level, "info");
            }
        });
        request(app)
            .get("/?sts=200")
            .end(function (err, res) {
                assert.ok(!err);
                assert.strictEqual(res.statusCode, 200);
                assert.strictEqual(count, 2);
                emitter.removeAllListeners();
                done();
            });
    });
    it("HTTP-400 should log warning-level message", function (done) {
        let count = 0;

        emitter.on("log", function (data) {
            count++;
            assert.ok(data.id);
            if (count === 1) {
                assertReq(data);
                assert.strictEqual(data.level, "info");
            } else if (count === 2) {
                assertRes(data);
                assert.strictEqual(data.level, "warning");
            }
        });
        request(app)
            .get("/?sts=400")
            .end(function (err, res) {
                assert.ok(!err);
                assert.strictEqual(res.statusCode, 400);
                assert.strictEqual(count, 2);
                emitter.removeAllListeners();
                done();
            });
    });
    it("HTTP-500 should log error-level message", function (done) {
        let count = 0;

        emitter.on("log", function (data) {
            count++;
            assert.ok(data.id);
            if (count === 1) {
                assertReq(data);
                assert.strictEqual(data.level, "info");
            } else if (count === 2) {
                assertRes(data);
                assert.strictEqual(data.level, "error");
            }
        });
        request(app)
            .get("/?sts=500")
            .end(function (err, res) {
                assert.ok(!err);
                assert.strictEqual(res.statusCode, 500);
                assert.strictEqual(count, 2);
                emitter.removeAllListeners();
                done();
            });
    });
});

after(function (done) {
    done();
});
