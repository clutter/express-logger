"use strict";

const HTTP_STATUS_INFO = 100;
const HTTP_STATUS_WARN = 400;
const HTTP_STATUS_ERROR = 500;

const defaults = {
    "id": false,
    "msgKey": "msg",
    "req": {
        "ignore": false,
        "level": "info",
        "serializer": function (req) { // eslint-disable-line func-names
            if (!req) {
                return false;
            }

            return {
                "url": req.url,
                "method": req.method,
                "httpVersion": req.httpVersion,
                "headers": req.headers,
                "query": req.query
            };
        }
    },
    "res": {
        "ignore": false,
        "level": function (req, res) { // eslint-disable-line func-names
            var level = "";

            if (res.statusCode >= HTTP_STATUS_INFO) {
                level = "info";
            }
            if (res.statusCode >= HTTP_STATUS_WARN) {
                level = "warning";
            }
            if (res.statusCode >= HTTP_STATUS_ERROR) {
                level = "error";
            }

            return level;
        },
        "serializer": function (req, res) { // eslint-disable-line func-names
            if (!res) {
                return false;
            }

            return {
                "statusCode": res.statusCode,
                "headers": res._headers
            };
        }
    }
};

function getValueByOpts(getter, req, res) {
    let data = getter;

    if (typeof getter === "function") {
        data = getter(req, res);
    }

    return data;
}

function logEvent(logger, msgKey, msg, id, refTime, key, opts, req, res) {
    let ignore = getValueByOpts(opts.ignore, req, res);
    let now = Date.now();
    let logData;

    if (!ignore) {
        logData = {};
        logData.id = id ? id : undefined;
        logData[msgKey] = msg;
        logData.ms = refTime ? now - refTime : undefined;
        logData[key] = getValueByOpts(opts.serializer, req, res);
        logger[getValueByOpts(opts.level, req, res)](logData);
    }

    return now;
}

function expressLogger(logger, options) {
    const opts = Object.assign({}, defaults, options);

    return function middleware(req, res, next) {
        var id = getValueByOpts(opts.id, req, res);
        var refTime;

        refTime = logEvent(logger, opts.msgKey, "HTTP Request", id, null, "req", opts.req, req, res);
        res.on("finish", function resFinish() {
            logEvent(logger, opts.msgKey, "HTTP Response", id, refTime, "res", opts.res, req, res);
        });
        next();
    };
}

module.exports = expressLogger;
