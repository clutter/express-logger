# Express Request/Response Logger
Logging middleware for ExpressJS

## Installation

```bash
$ npm install @parthar/express-logger --save
```

## Usage

```js
// express setup
var express = require("express");
var app = express();
var router = require("./my-app-router");
// provides middleware for expressjs request logging
var expressLogger = require("@parthar/express-logger");
// use expressLogger(options) to create middleware
app.use(expressLogger(logger, options));
// now setup routes
app.use(router);
```

## Options
Use any logger-instance that supports the levels as returned by "level" functions. Each of the options can either be a function or the target-value. (Be cautious if/when mutating req/res objects.)

```js
{
    id: function (req, res) { return String; }, // log-id; useful to track req/res
    msgKey: "msg", // key to use for "HTTP Request/Response" message
    req: {
        ignore: function (req, res) { return Boolean; }, // return true to skip logging
        level: function (req, res) { return String; }, // return supported log-level by loggerInstance
        serializer: function (req, res) { return Object; }, // return req JSON-obj to log
    },
    req: {
        ignore: function (req, res) { return Boolean; }, // return true to skip logging
        level: function (req, res) { return String; }, // return supported log-level by loggerInstance
        serializer: function (req, res) { return Object; }, // return req JSON-obj to log
    }
}
```

The default options are as follows:
```js
{
    "id": false,
    "msgKey": "msg",
    "req": {
        "ignore": false,
        "level": "info",
        "serializer": function (req) {
            if (!req) {
                return false;
            }
            return {
                "url": req.url,
                "method": req.method,
                "httpVersion": req.httpVersion,
                "headers": req.headers,
                "query": req.query
            };
        }
    },
    "res": {
        "ignore": false,
        "level": function (req, res) { // eslint-ignore-line func-names
            var level = "";
            if (res.statusCode >= 100) {
                level = "info";
            }
            if (res.statusCode >= 400) {
                level = "warning";
            }
            if (res.statusCode >= 500) {
                level = "error";
            }
            return level;
        },
        "serializer": function (req, res) { // eslint-ignore-line func-names
            if (!res) {
                return (false);
            }
            return ({
                statusCode: res.statusCode,
                headers: res._headers
            });
        }
    }
}
```
